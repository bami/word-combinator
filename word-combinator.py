"""
    word-combinator
    ~~~~~~~~~~~~~~~
    :copyright: (c) 2015 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :desription: word combinator

Copyright (c) 2015 by Bastian Migge.  See AUTHORS
for more details.

Some rights reserved.

Redistribution and use in source and binary forms of the software as well
as documentation, with or without modification, are permitted provided
that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the following
  disclaimer in the documentation and/or other materials provided
  with the distribution.

* The names of the contributors may not be used to endorse or
  promote products derived from this software without specific
  prior written permission.

THIS SOFTWARE AND DOCUMENTATION IS PROVIDED BY THE COPYRIGHT HOLDERS AND
CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE AND DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.
"""

import sys
import string

def comblist(matrix):
    """ recursive callback to generate combinations of matrix rows"""
    if len(matrix) == 1:
        return matrix[0]
    else:
        result = []
        for l in matrix[0]:
            for i in comblist(matrix[1:]):
                result.append("%s%s" % (l,i))
        return result

def readMatrixFromFile(filename):
    """ return value matrix from file
    new row if empty line
    """
    f = open(filename)
    text = f.read()
    entries = string.split(text,"\n")
    matrix = []
    row = []
    for entry in entries:
        if entry == "":
            if len(row):
                matrix.append(row)
                row = []
        else:
            row.append(entry) 

    return matrix

def usage():
    print("usage: %s [-h -v] -w <wordlistfilename>" % sys.argv[0])


if __name__ == "__main__":
    if len(sys.argv) < 2 or sys.argv[1] == '-h':
        usage()

    elif sys.argv[1] == '-v':
        print('''word-combinator v1.0
written by Bastian Migge <bastian@migge.info>
http://www.migge.info
Take a lock at README.rst file for detailed information
''')

    elif sys.argv[1] == '-w':
        if len(sys.argv) < 3:
            print('no wordfile specified')
            usage()
        else:
            matrix = readMatrixFromFile(sys.argv[2])
            liste = comblist(matrix)
            for l in liste:
                print(l)
    else:
        usage()
