word-combinator v1.0
====================

word-combinator generates combinations of two or more word lists, 
with respect to the ordering of the lists.

Examples: given the word lists 'fizz', 'buzz' and 'hello', 'world'
the result is: 'fizzhello','fizzworld','buzzhello', 'buzzworld'

wordlist.txt:
*************
fizz
buzz

hello
world

Call:
*****
python ./word-combinator.py -w wordlist.txt


